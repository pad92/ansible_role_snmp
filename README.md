![build status](https://git.depad.fr/ansible/roles/snmp/badges/master/pipeline.svg)


# snmp

Configure snmp daemon

## Requirements

none

## Role Variables

```
snmp:
  servers:
    - { address: "127.0.0.1" }
  community: cts
```

## Dependencies

none

## Example Playbook

```
- hosts: all
  gather_facts: False
  pre_tasks:
    - name: pre_tasks | setup python
      raw: command -v yum >/dev/null && yum -y install python python-simplejson libselinux-python redhat-lsb || true ; command -v apt-get >/dev/null && sed -i '/cdrom/d' /etc/apt/sources.list && apt-get update && apt-get install -y python python-simplejson lsb-release aptitude || true
      changed_when: False
    - name: pre_tasks | gets facts
      setup:
      tags:
      - always

  roles:
    - snmp
```

